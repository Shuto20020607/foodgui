import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea textArea1;
    private JButton curryButton;
    private JButton sushiButton;
    private JButton sobaButton;
    private JButton checkOutButton;
    private JLabel total;
    private JButton clearButton;
    private JTabbedPane tabbedPane1;
    private JButton appleButton;
    private JButton orangeButton;
    private JButton oolongTeaButton;
    private JButton colaButton;
    private JButton gingerAleButton;
    private JButton coffeeButton;
    private JButton riceButton;
    private JButton potatoButton1;
    private JButton saladButton;
    private JButton tiramisuButton;
    private JButton iceButton;
    private JButton cakeButton;
    private int totalprice = 0;
    private JFrame JFrameTest;

    void order(String food, int price1, int price2){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0) {
            int confirmation2 = JOptionPane.showConfirmDialog(null,
                    "Are you taking it home?", "Take out", JOptionPane.YES_NO_OPTION);
            if (confirmation2 == 1) {
                JOptionPane.showMessageDialog(null,
                        "Order for " + food + " received.");
            }
            else if(confirmation2==0){
                JOptionPane.showMessageDialog(null,
                        "Order for " + food + " received.(Take out.)");
            }
            String currentText = textArea1.getText();
            if(confirmation2==1) {
                textArea1.setText(currentText + food + "   " + price1 + "yen\n");
                totalprice+=price1;
                total.setText("Total  " + totalprice +"  yen");
            }
            else if(confirmation==0){
                textArea1.setText(currentText + food + "   " + price2 + "yen\n");
                totalprice+=price2;
                total.setText("Total  " + totalprice +"  yen");
            }
        }
    }

    void order2(String food, int price1, int price2){
        int confirmation3 = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation3 == 0) {
            int confirmation4 = JOptionPane.showConfirmDialog(null,
                    "Are you taking it home?", "Take out", JOptionPane.YES_NO_OPTION);
            if (confirmation4 == 1) {
                JOptionPane.showMessageDialog(null,
                        "Order for " + food + " received.");
            }
            else if(confirmation4==0){
                JOptionPane.showMessageDialog(null,
                        "Order for " + food + " received.(Take out.)");
            }
            String currentText = textArea1.getText();
            if(confirmation4==1) {
                textArea1.setText(currentText + food + "   " + price1 + "yen\n");
                totalprice+=price1;
                total.setText("Total  " + totalprice +"  yen");
            }
            else if(confirmation3==0){
                textArea1.setText(currentText + food + "   " + price2 + "yen\n");
                totalprice+=price2;
                total.setText("Total  " + totalprice +"  yen");
            }
            if (confirmation4 == 1) {
                int confirmation5 = JOptionPane.showConfirmDialog(null,
                        "Would you like to have it after dinner?",
                        "Timing to bring",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation5 == 0) {
                    JOptionPane.showMessageDialog(null,
                            "We will bring you " + food + " after your meal.");
                }
            }
        }
    }
    public FoodGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",990,972);
            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.jpg")
        ));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",880,864);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ramen.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",550,540);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry",770,756);
            }
        });
        curryButton.setIcon(new ImageIcon(
                this.getClass().getResource("curry.jpg")
        ));
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",1100,1080);
            }
        });
        sushiButton.setIcon(new ImageIcon(
                this.getClass().getResource("sushi.jpg")
        ));
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",660,648);
            }
        });
        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("soba.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation =JOptionPane.showConfirmDialog(null,
                        " Would you like to checkout?" ,"Checkout Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you.The total price is "
                            + totalprice +" yen.");
                    textArea1.setText("　");
                    totalprice =0;
                    total.setText(totalprice +" yen");
                }
            }
        });
        total.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
            }
        });
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation =JOptionPane.showConfirmDialog(null,
                        " Would you like to clear?" ,"Clear Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation==0) {
                    textArea1.setText("　");
                    totalprice = 0;
                    total.setText(totalprice +" yen");
                }
            }
        });
        appleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("apple",143,140);
            }
        });
        appleButton.setIcon(new ImageIcon(
                this.getClass().getResource("apple.jpg")
        ));
        orangeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("orange",154,151);
            }
        });
        orangeButton.setIcon(new ImageIcon(
                this.getClass().getResource("orange.jpg")
        ));
        oolongTeaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("oolong tea",165,162);
            }
        });
        oolongTeaButton.setIcon(new ImageIcon(
                this.getClass().getResource("tea.jpg")
        ));
        colaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("cola",176,173);
            }
        });
        colaButton.setIcon(new ImageIcon(
                this.getClass().getResource("cola.jpg")
        ));
        gingerAleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ginger ale",198,194);
            }
        });
        gingerAleButton.setIcon(new ImageIcon(
                this.getClass().getResource("gingerale.jpg")
        ));
        coffeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("coffee",220,216);
            }
        });
        coffeeButton.setIcon(new ImageIcon(
                this.getClass().getResource("coffee.jpg")
        ));

        textArea1.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);

            }
        });
        riceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("rice",110,108);
            }
        });
        riceButton.setIcon(new ImageIcon(
                this.getClass().getResource("rice.jpg")
        ));
        potatoButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("potato",440,432);
            }
        });
        potatoButton1.setIcon(new ImageIcon(
                this.getClass().getResource("potato.jpg")
        ));
        saladButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("salad",330,324);
            }
        });
        saladButton.setIcon(new ImageIcon(
                this.getClass().getResource("salad.jpg")
        ));
        tiramisuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("tiramisu",385,378);
            }
        });
        tiramisuButton.setIcon(new ImageIcon(
                this.getClass().getResource("tiramisu.jpg")
        ));
        iceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("ice",231,227);
            }
        });
        iceButton.setIcon(new ImageIcon(
                this.getClass().getResource("ice.jpg")
        ));
        cakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("cake",352,346);
            }
        });
        cakeButton.setIcon(new ImageIcon(
                this.getClass().getResource("cake.jpg")
        ));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
